import styled from 'styled-components';

export const StyledChat = styled.div.attrs(props => ({
  className: 'ma2 pa2 flex-row'
}))`
  height: 75vh;
  width: 500px;
`;

