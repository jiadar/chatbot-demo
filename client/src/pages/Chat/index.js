import React from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { StyledChat } from './styles';
import ContactProfile from '../../components/ContactProfile';
import ChatComposer from '../../components/ChatComposer';
import MessagePane from '../../components/MessagePane';
import chatQuery from '../../graphql/Chats/queries.gql';

const Chat = () => {
  const { loggedInUser } = useParams();

  // load the only chat in the database by ID, this is a hack for
  // demo purposes only - should also check error at some point.
  // Generally, This should be in useEffect hook so we can show
  // the real UI as possible rather than Loading. It's a demo though.
  const { loading, data } = useQuery(chatQuery, {
    variables: { id: '5df316de29b28fa9fb6672fc' },
  });

  if (loading) return <p>Loading...</p>;

  // kind of a hack to get around not writing much user logic
  // but still have multi-user simulated experience for demo
  let me;
  let them;
  data.chat.participants.forEach((user) => {
    if (user._id !== loggedInUser) them = user;
    else me = user;
  });

  // set up the chat window in the center of the screen, and populate it
  // with the contact info, a message window, and an input field
  return (
    <div className="flex items-center justify-center w-100">
      <StyledChat>
        <ContactProfile recipient={them.fn} />
        <MessagePane
          messages={data.chat.messages}
          chatid={data.chat._id}
          loggedInUser={loggedInUser}
        />
        <ChatComposer sender={me} recipient={them} />
      </StyledChat>
    </div>
  );
};

export default Chat;
