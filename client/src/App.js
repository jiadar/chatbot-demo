import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from './pages/Login/';
import Chat from './pages/Chat';

// Start at the / which will display a list of users via the Login component
// After which we will pass the id of the logged in user as a param. Kind of
// hack to get around not having to write any user logic but still show
// chat function between multiple users
const App = () => (
  <div>
    <Router>
      <Switch>
        <Route path="/chat/:loggedInUser">
          <Chat />
        </Route>
        <Route path="/">
          <Login />
        </Route>
      </Switch>
    </Router>
  </div>
);


export default App;
