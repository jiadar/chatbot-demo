import styled from 'styled-components';

export const StyledContactProfile = styled.div.attrs(props => ({
  className: 'w-100'
}))`
  height: 60px;
  line-height: 60px;
  background: #f5f5f5;
  img {
    width: 40px;
    border-radius: 50%;
    float: left;
    margin: 9px 12px 0 9px;
  }
  p {
    float: left;  
  }
`;

export default StyledContactProfile;
