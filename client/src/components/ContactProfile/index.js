import React from 'react';
import PropTypes from 'prop-types';
import { StyledContactProfile } from './styles';

// This shows a contact profile - the information about the contact
// we are talking with. It could be the avatar image, name, or other
// profile details. Now we just use a dummy avatar image.

const ContactProfile = ({ recipient }) => (
  <StyledContactProfile>
    <img
      src="http://jump.javin.io/assets/person2.png"
      alt="Recipient"
    />
    <p>{recipient}</p>
  </StyledContactProfile>
);

export default ContactProfile;

ContactProfile.propTypes = {
  recipient: PropTypes.string.isRequired,
};
