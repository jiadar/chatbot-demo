import styled from 'styled-components';

export const Sent = styled.div`
  img {
    margin: 6px 8px 0 0;
    float: left;
  }
  p {
    background: #435f7a;
    color: #f5f5f5;
  }
`;

export const Reply = styled.div`
  img {
    float: right;
    margin: 6px 0 0 8px;
  }
  p {
    background: #f5f5f5;
    float: right;  
  }
`;
