import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useMutation, useSubscription } from '@apollo/react-hooks';
import { StyledMessagePane } from './styles';
import Message from '../Message';
import messagesSubscription from '../../graphql/Messages/subscriptions.gql';
import ADD_MESSAGE from '../../graphql/Messages/mutations.gql';
import { validateResponse, lookupBotMessageById } from './functions';

const { fetchUrl } = require('fetch');

const MessagePane = ({ messages, loggedInUser }) => {
  // get the bot data from the static server
  const BOT_DATA_URL = 'http://localhost:4000/botmessages.json';

  // api server hook to add new messages
  const [addMessage] = useMutation(ADD_MESSAGE);

  // should useReducer here, or redux, but this is less boilerplate
  const [messageList, setMessageList] = useState([]);
  const [botMessages, setBotMessages] = useState([]);
  const [botIndex, setBotIndex] = useState(1);
  const [botCompleted, setBotCompleted] = useState(false);
  const endOfChatRef = useRef(null);

  // Send a message as the bot. Take as input the ID of the user
  // that is the bot and the index of the bot message. Compute if
  // the bot has completed it's questioning, and if so, set the
  // state for that. If not, add the bot's next message via
  // mutation api call.
  const sendBotMessage = (botSenderId, nextBotIndex) => {
    setBotIndex(nextBotIndex);
    if (nextBotIndex === 9 || nextBotIndex === 0) setBotCompleted(true);
    if (nextBotIndex < botMessages.length) {
      addMessage({
        variables: {
          message: {
            text: botMessages[nextBotIndex].question,
            senderId: botSenderId,
            recipientId: loggedInUser,
          },
        },
      });
    }
  };

  // Messages subscription. Provide a callback function when a subscription
  // message comes in, which does the following.
  // - add the message to the state containing all the messages
  //   this also will update UI with the new messages
  // - Scroll the messages window so that the latest message is visible
  // - Send a bot message if the last message came from the user
  // - Bot messages will trigger a subscription update as well
  //
  // Sending a bot message involves the following.
  // - Check if the bot has completed it's questioning, if it has, don't
  //   send any more
  // - Validate the response that the user provided. This data is available
  //   from the subscription data
  // - As the bot ID's don't match the array indexes, and pathing is used
  //   to determine the next question, we need to look up the array index
  //   of the next question
  // - Finally, send the message at the computed index from the bot
  //   to the recipient
  useSubscription(messagesSubscription, {
    onSubscriptionData: ({ subscriptionData }) => {
      const newMessage = subscriptionData.data.messageAdded;
      setMessageList((prev) => [...prev, newMessage]);
      endOfChatRef.current.scrollIntoView();
      if (newMessage.sender._id === loggedInUser
         && !botCompleted) {
        const { data } = validateResponse(botMessages[botIndex], newMessage.text);
        const nextBotIndex = lookupBotMessageById(botMessages, data.nextMessage);
        sendBotMessage(newMessage.recipient._id, nextBotIndex);
      }
    },
  });

  // This effect runs on the first render only, due to the empty array
  // of dependencies. The effect sets up the initial state by:
  // - Querying the URL for the bot question data
  // - Populating the message state with messages from the backend
  // Note that we should load the BOT_DATA_URL into state and ensure
  // it has loaded before we access it, but we're depending on the user
  // to be slower than the api call. (bad)
  useEffect(() => {
    fetchUrl(BOT_DATA_URL, (error, meta, body) => {
      if (!error) setBotMessages(JSON.parse(body));
    });
    messages.forEach((newMessage) => {
      setMessageList((prev) => [...prev, newMessage]);
    });
  },[]); // eslint-disable-line

  // Render the message pane. We have styled ul / li to present the messages
  // in a readable form. We set an align parameter so that we can put the
  // senders messages on one side and the recipient's on the other. At
  // the end, we create a div with a ref in order to scroll to the bottom
  // when new messages are received by the subscription.
  return (
    <StyledMessagePane>
      <ul>
        {messageList.map((message) => {
          const align = message.sender._id === loggedInUser ? 'right' : 'left';
          return (
            <Message
              sender={message.sender.fn}
              align={align}
              dateSent={message.dateSent}
              content={message.text}
              key={message._id}
            />
          );
        })}
        <li>
          <div ref={endOfChatRef} />
        </li>
      </ul>
    </StyledMessagePane>
  );
};

export default MessagePane;

MessagePane.propTypes = {
  messages: PropTypes.array.isRequired,
  loggedInUser: PropTypes.string.isRequired,
};
