import styled from 'styled-components';

export const StyledMessagePane = styled.div`
  height: 100%;
  width: 100%;
  overflow-y: auto;
  margin: 0;
  padding: 0;
  ul li {
    display: inline-block;
    clear: both;
    float: left;
    margin: 15px 15px 5px 15px;
    width: calc(100% - 35px);
    font-size: 0.9em;
    img {
      width: 22px;
      border-radius: 50%;
    }
    p {
      display: inline-block;
      padding: 10px 15px;
      border-radius: 20px;
      line-height: 130%;
      max-width: calc(100% - 35px); 
   } 
  }
  > *:last-child {
    margin-bottom: 20px;
  }
`;

