# datacreators.py
#
# This calls record fake methods and adds the fake records to the underlying database/connection
# making cross references for joins where necessary.
#

from pprint import pprint
import random
from datagenerators import fake_user, fake_chat, fake_message
from pydash import py_

NUM_USERS = 2
MIN_CHATS = 1
MAX_CHATS = 1
MIN_MESSAGES = 2      # this is per chat
MAX_MESSAGES = 5

def create_data(db, fake):
    userids = create_users(db, fake)
    chats = create_chats(db, fake, userids)
    messages = create_messages(db, fake, chats)

def create_users(db, fake):
    users = []
    for i in range(NUM_USERS):
        userid = db.users.insert_one(fake_user(fake)).inserted_id
        users.append(userid)
    return users

def create_chats(db, fake, users):
    chats = []
    for _ in range(random.SystemRandom().randint(MIN_CHATS, MAX_CHATS)):
        data = fake_chat(fake, users);
        chatid = db.chats.insert_one(data).inserted_id
        chats.append({ id: chatid, **data })
    return chats

def create_messages(db, fake, chats):
    messages = []
    for chat in chats: 
        for _ in range(random.SystemRandom().randint(1, MAX_MESSAGES)):
            message = fake_message(fake, chat)
            messageid = db.messages.insert_one(message).inserted_id
            messages.append(messageid)
    return messages

def print_results(db):
    print()
    pprint(db.users.find_one())
    pprint(db.chats.find_one())
    pprint(db.messages.find_one())
