# seed.py
#
# A python3 seed program for loading fake data to be used for performance testing.
# All collections will be dropped in the connected database before creating new fake data. 
##
# After running it will print one record from each database for verification.
#
# python3 seed.py [# of chats] [# of messages]
#

import sys
from faker import Faker
from pydash import py_
from datacreators import create_data, print_results
from config import get_client

client = get_client()
db = client.chatdemo
fake = Faker()

def setup_collections():
    db.users.delete_many({});
    db.chats.delete_many({});
    db.messages.delete_many({});
    
if __name__ == '__main__':
    fake = Faker()
    setup_collections()
    create_data(db, fake)
    print_results(db)
    


