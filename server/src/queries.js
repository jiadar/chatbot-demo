/* eslint-disable */

// Documentation only - may be out of date
// Some query examples

mutation addMessage($message: MessageInput) {
  addMessage(message: $message) {
    _id
    text
    dateSent
    sender {
      fn
    }
    recipient {
      fn
    }
  }
}

{
  "message": {
    "text": "Test Message",
    "senderId": "5de3373dd6ba600b79b658db",
    "recipientId": "5de3373dd6ba600b79b658db"
  }
}

mutation addUser($user: UserInput) {
  addUser(user: $user) {
    _id
    email
    fn
    ln   
  } 
}

{
  "user": {
    "username":"test",
    "email":"test@test.io",
    "fn":"Test",
    "ln":"Er"                
  }
}

query {
  messages {
    _id
    text
    dateSent
    sender {
      fn
    }
    recipient {
      fn
    } 
  } 
}
