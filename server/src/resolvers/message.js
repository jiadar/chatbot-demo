import Message from '../models/Message';
import messageController from '../controllers/message';
import User from '../models/User';

const { PubSub } = require('apollo-server-express');

const MESSAGE_ADDED = 'MESSAGE_ADDED';
const pubsub = new PubSub();

export default {
  Message: {
    __resolveReference(object) {                      
      return Message.findOne({ _id: object.id });
    },
    sender(message) {
      return User.findById(message.senderId);
    },
    recipient(message) {
      return User.findById(message.recipientId);
    },
  },
  Query: {
    message(_, args) {
      return Message.findOne({ _id: args.id });
    },
    messages() {
      return Message.find({}).sort({ dateSent: 1 });
    },
  },
  Mutation: {
    async addMessage(parent, args) {
      const newMessage = await messageController.addMessage(args);
      pubsub.publish(MESSAGE_ADDED, { messageAdded: newMessage });
      return newMessage;
    },
  },
  Subscription: {
    messageAdded: {
      subscribe: () => pubsub.asyncIterator([MESSAGE_ADDED]),
    },
  },
};
