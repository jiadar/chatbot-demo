import User from '../models/User';
import userController from '../controllers/user';
import Chat from '../models/Chat';

export default {
  User: {
    __resolveReference(object) {                  
      return User.findOne({ _id: object.id });
    },
    chats(user) {
      return Chat.find({ participants: user._id });
    },
  },
  Query: {
    user(_, args) {
      return User.findOne({ _id: args.id });
    },
    users() {
      return User.find({});
    },
  },
  Mutation: {
    addUser(parent, args) {
      return userController.addUser(args);
    },
  },
};
