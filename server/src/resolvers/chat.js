import Chat from '../models/Chat';
import User from '../models/User';
import Message from '../models/Message';

export default {
  Chat: {
    __resolveReference(object) {
      return Chat.findOne({ _id: object.id });
    },
    participants(chat) {
      const users = [];
      chat.participants.forEach((elt) => {
        const user = User.findById(elt);
        users.push(user);
      });
      return users;
    },
    messages(chat) {
      return Message.find(
        {
          $or:
          [
            {
              senderId: {
                $in: chat.participants,
              },
            },
            {
              recipientId: {
                $in: chat.participants,
              },
            },
          ],
        },
      ).sort({ dateSent: 1 });
    },
  },
  Query: {
    chat(_, args) {
      return Chat.findOne({ _id: args.id });
    },
    chats() {
      return Chat.find({}).sort({ lastAccessed: 1 });
    },
  },
};
