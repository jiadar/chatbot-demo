import { mergeResolvers } from 'merge-graphql-schemas';
import Chat from './chat';
import Message from './message';
import User from './user';

const resolvers = [
  User,
  Chat,
  Message,
];

export default mergeResolvers(resolvers);
