export default `
  type Message {
    _id: ID!
    text: String
    dateSent: String
    dateReceived: String
    read: String
    sender: User
    recipient: User
  }

  input MessageInput {
    text: String
    senderId: ID!
    recipientId: ID!
  }

  type Query {
    message(id: String): Message
    messages: [Message]
  }

  type Mutation {
    addMessage(message: MessageInput): Message
  }

  type Subscription {
    messageAdded: Message
  }
`;
