// types/index.js
//
// This file merges all the types in this directory, building up to schema creation. If you add a new type
// you will have to import it here and append it to typeDefs. 
//

import { mergeTypes } from 'merge-graphql-schemas';

import Chat from './chat';
import Message from './message';
import User from './user';

const types =  [
  Message,
  Chat,
  User,
];

export default mergeTypes(types, { all: true });
