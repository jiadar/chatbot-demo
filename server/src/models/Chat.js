import mongoose from 'mongoose';

const ChatSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  lastAccessed: String,
  participants: Array,
});

export default mongoose.model('Chat', ChatSchema);
