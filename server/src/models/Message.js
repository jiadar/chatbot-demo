import mongoose from 'mongoose';

const MessageSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  senderId: mongoose.Schema.ObjectId,
  recipientId: mongoose.Schema.ObjectId,
  text: String,
  dateSent: String,
  dateReceived: String,
  read: Boolean,
});

export default mongoose.model('Message', MessageSchema);
