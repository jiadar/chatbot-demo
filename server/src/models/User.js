import mongoose from 'mongoose';

const UserSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  username: String,
  email: String,
  fn: String,
  ln: String,
});

export default mongoose.model('User', UserSchema);
