import mongoose from 'mongoose';
import express from 'express';
import http from 'http';
import { ApolloServer } from 'apollo-server-express';
import User from './models/User';
import Chat from './models/Chat';
import Message from './models/Message';
import config from './config';
import typeDefs from './types';
import resolvers from './resolvers';

const cors = require('cors')
const path = require('path');

const PORT = 4000;
const app = express();

// Use cors with an origin of our client in order to serve static files
// Send a dummy page on root, as we will serve static files there from
// the public directory. 
//
// Use the Mongoose ORM to connect to the underlying local mongo database.
// No security was set up for the demo. Send the ORM models in the context.
//
// Start a new apollo server which executes the graphql queries. Connect to the engine to monitor
// performance and publish schemas. Allow introspection so the engine and playground can obtain the
// API specification. Also log data and errors to the console, which we should
// only do in development.
//
// Install the apollo server on the /api endpoint. Start up the server
// and websocket gateway. 


app.use(cors({ origin: 'http://localhost:3000' }));
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', (req, res) => { 
  res.send('Apollo and static express server');
});

const db = config.mongodb;

const opts = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

mongoose
  .connect(db, opts)
  .then(() => console.log('MongoDB connected')) //eslint-disable-line
  .catch((err) => console.log(err));            //eslint-disable-line

const context = {
  User,
  Chat,
  Message,
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  //  engine: { apiKey: 'GET_API_KEY_FROM_ENV' },
  introspection: true,
  playground: true,
  context,
  subscriptions: '/',
  formatError: (error) => {
    console.log(error);            // eslint-disable-line
    return error;
  },
  formatResponse: (response) => {
    console.log(response);         // eslint-disable-line
    return response;
  },
});

server.applyMiddleware({ app, path: '/api' });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: PORT }, () => {
  // eslint-disable-next-line
  console.log(`Server ready at http://localhost:${PORT}${server.graphqlPath}`);
  // eslint-disable-next-line
  console.log(`Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`);
});
