import bson from 'bson';
import User from '../models/User';

const addUser = (args) => {
  const _id = new bson.ObjectId();
  const newRecord = new User({ _id, ...args.user });
  return newRecord.save();
};

const userController = {
  addUser,
};

export default userController;
