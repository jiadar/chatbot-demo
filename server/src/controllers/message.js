import bson from 'bson';
import Message from '../models/Message';

const addMessage = (args) => {
  const _id = new bson.ObjectId();
  const dateSent = Date.now() / 1000;
  const newRecord = new Message({ _id, dateSent, ...args.message });
  return newRecord.save();
};

const messageController = {
  addMessage,
};

export default messageController;
